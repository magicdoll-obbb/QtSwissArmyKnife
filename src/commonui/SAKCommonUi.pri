INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/SAKAffixesComboBox.hh \
    $$PWD/SAKBaudRateComboBox.hh \
    $$PWD/SAKBluetoothDeviceInfoComboBox.hh \
    $$PWD/SAKCheckBox.hh \
    $$PWD/SAKComboBox.hh \
    $$PWD/SAKCrcAlgorithmComboBox.hh \
    $$PWD/SAKDataBitsComboBox.hh \
    $$PWD/SAKEscapeCharacterComboBox.hh \
    $$PWD/SAKFlowControlComboBox.hh \
    $$PWD/SAKIpComboBox.hh \
    $$PWD/SAKLineEdit.hh \
    $$PWD/SAKMenu.hh \
    $$PWD/SAKParityComboBox.hh \
    $$PWD/SAKPortNameComboBox.hh \
    $$PWD/SAKResponseOptionComboBox.hh \
    $$PWD/SAKSpinBox.hh \
    $$PWD/SAKStopBitsComboBox.hh \
    $$PWD/SAKTextFormatComboBox.hh \
    $$PWD/SAKUiInterface.hh \
    $$PWD/SAKWebSocketMessageTypeComboBox.hh

SOURCES += \
    $$PWD/SAKAffixesComboBox.cc \
    $$PWD/SAKBaudRateComboBox.cc \
    $$PWD/SAKBluetoothDeviceInfoComboBox.cc \
    $$PWD/SAKCheckBox.cc \
    $$PWD/SAKComboBox.cc \
    $$PWD/SAKCrcAlgorithmComboBox.cc \
    $$PWD/SAKDataBitsComboBox.cc \
    $$PWD/SAKEscapeCharacterComboBox.cc \
    $$PWD/SAKFlowControlComboBox.cc \
    $$PWD/SAKIpComboBox.cc \
    $$PWD/SAKLineEdit.cc \
    $$PWD/SAKMenu.cc \
    $$PWD/SAKParityComboBox.cc \
    $$PWD/SAKPortNameComboBox.cc \
    $$PWD/SAKResponseOptionComboBox.cc \
    $$PWD/SAKSpinBox.cc \
    $$PWD/SAKStopBitsComboBox.cc \
    $$PWD/SAKTextFormatComboBox.cc \
    $$PWD/SAKUiInterface.cc \
    $$PWD/SAKWebSocketMessageTypeComboBox.cc
