INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/SAKModbusClient.hh \
    $$PWD/SAKModbusDevice.hh \
    $$PWD/SAKModbusFactory.hh \
    $$PWD/SAKModbusRtuSerialClient.hh \
    $$PWD/SAKModbusRtuSerialServer.hh \
    $$PWD/SAKModbusServer.hh \
    $$PWD/SAKModbusTcpClient.hh \
    $$PWD/SAKModbusTcpServer.hh

SOURCES += \
    $$PWD/SAKModbusClient.cc \
    $$PWD/SAKModbusDevice.cc \
    $$PWD/SAKModbusFactory.cc \
    $$PWD/SAKModbusRtuSerialClient.cc \
    $$PWD/SAKModbusRtuSerialServer.cc \
    $$PWD/SAKModbusServer.cc \
    $$PWD/SAKModbusTcpClient.cc \
    $$PWD/SAKModbusTcpServer.cc

