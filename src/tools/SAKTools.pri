INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/SAKAnalyzerTool.hh \
    $$PWD/SAKBaseTool.hh \
    $$PWD/SAKBleCentralTool.hh \
    $$PWD/SAKCommunicationTool.hh \
    $$PWD/SAKEmitterTool.hh \
    $$PWD/SAKMaskerTool.hh \
    $$PWD/SAKPrestorerTool.hh \
    $$PWD/SAKResponserTool.hh \
    $$PWD/SAKSerialPortTool.hh \
    $$PWD/SAKSerialPortTransmitterTool.hh \
    $$PWD/SAKSocketClientTool.hh \
    $$PWD/SAKSocketClientTransmitterTool.hh \
    $$PWD/SAKSocketServerTool.hh \
    $$PWD/SAKStatisticianTool.hh \
    $$PWD/SAKStorerTool.hh \
    $$PWD/SAKTableModelTool.hh \
    $$PWD/SAKTcpClientTool.hh \
    $$PWD/SAKTcpServerTool.hh \
    $$PWD/SAKTcpTransmitterTool.hh \
    $$PWD/SAKToolFactory.hh \
    $$PWD/SAKTransmitterTool.hh \
    $$PWD/SAKUdpClientTool.hh \
    $$PWD/SAKUdpServerTool.hh \
    $$PWD/SAKUdpTransmitterTool.hh \
    $$PWD/SAKVelometerTool.hh \
    $$PWD/SAKWebSocketClientTool.hh \
    $$PWD/SAKWebSocketServerTool.hh \
    $$PWD/SAKWebSocketTransmitterTool.hh

SOURCES += \
    $$PWD/SAKAnalyzerTool.cc \
    $$PWD/SAKBaseTool.cc \
    $$PWD/SAKBleCentralTool.cc \
    $$PWD/SAKCommunicationTool.cc \
    $$PWD/SAKEmitterTool.cc \
    $$PWD/SAKMaskerTool.cc \
    $$PWD/SAKPrestorerTool.cc \
    $$PWD/SAKResponserTool.cc \
    $$PWD/SAKSerialPortTool.cc \
    $$PWD/SAKSerialPortTransmitterTool.cc \
    $$PWD/SAKSocketClientTool.cc \
    $$PWD/SAKSocketClientTransmitterTool.cc \
    $$PWD/SAKSocketServerTool.cc \
    $$PWD/SAKStatisticianTool.cc \
    $$PWD/SAKStorerTool.cc \
    $$PWD/SAKTableModelTool.cc \
    $$PWD/SAKTcpClientTool.cc \
    $$PWD/SAKTcpServerTool.cc \
    $$PWD/SAKTcpTransmitterTool.cc \
    $$PWD/SAKToolFactory.cc \
    $$PWD/SAKTransmitterTool.cc \
    $$PWD/SAKUdpClientTool.cc \
    $$PWD/SAKUdpServerTool.cc \
    $$PWD/SAKUdpTransmitterTool.cc \
    $$PWD/SAKVelometerTool.cc \
    $$PWD/SAKWebSocketClientTool.cc \
    $$PWD/SAKWebSocketServerTool.cc \
    $$PWD/SAKWebSocketTransmitterTool.cc
